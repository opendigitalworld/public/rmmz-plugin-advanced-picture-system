# Advanced Picture System Plugin for RPG Maker MZ

This plugin lets you manipulate images or text transformed into images by means of various transformations: moving, scaling, rotating, spinning, flipping vertically or horizontally, fading and tinting. Texts can also include icons and other text commands (e.g. \C[x], \{ or \}, ...). Finally, this plugin enables common events to be triggered by various mouse or keyboard actions (hover, press, click). 

## Patches

_This plugin doesn't have patches._

## Plugin Settings

- **Variable ID (Picture CE Trigger)** [number]: The variable ID reserved for recording the ID of the picture triggering a common event.

## Plugin Commands

### Quick Create Image

Displays an image as a picture with default system settings.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Image** [file]: The image file used as a picture.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

### Quick Create Text

Displays a text as a picture with default system settings.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Text** [file]: The text to display as a picture.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

### Quick Create Animation

Displays a sprite as an animated picture with default system settings.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Sprite** [file]: The sprite file used as a picture.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Animation Frames** [number]: The number of frames that make up the animated picture. A number between 1 and 100.

- **Animation Duration** [number]: The number of frames the animated picture will take to complete a full animation. A number between 0 and 3600.

### Quick Bind Common Event

Binds a common event to an existing picture.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Common Event Type** [list]: The common event type to call.

- **Common Event ID** [string]: The common event ID to call. Can use a variable v[id] or a number between 1 and 999.

### Create Image

Displays an image as a picture.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Image** [file]: The image file used as a picture.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Scale Width** [number]: The scale width of the picture. A number between 0 and 2000.

- **Scale Height** [number]: The scale height of the picture. A number between 0 and 2000.

- **Rotation Angle** [number]: The rotation angle of the picture. A number between 0 and 360.

- **Opacity** [number]: The opacity of the picture. A number between 0 and 255.

- **Blend Mode** [list]: The blend mode of the picture.

- **Common Event**: On Mouse Enter [common event]: The common event to call when the picture is hovered by the mouse.

- **Common Event**: On Mouse Exit [common event]: The common event to call when the picture is no more hovered by the mouse.

- **Common Event: On Press** [common event]: The common event to call when the picture is pressed.

- **Common Event: On Click** [common event]: The common event to call when the picture is clicked.

### Create Text

Displays a text as a picture.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Text** [file]: The text to display as a picture.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Scale Width** [number]: The scale width of the picture. A number between 0 and 2000.

- **Scale Height** [number]: The scale height of the picture. A number between 0 and 2000.

- **Rotation Angle** [number]: The rotation angle of the picture. A number between 0 and 360.

- **Opacity** [number]: The opacity of the picture. A number between 0 and 255.

- **Blend Mode** [list]: The blend mode of the picture.

- **Font Name** [string]: The font name of the text picture. A value from the list of custom fonts (leave blank for default font name).

- **Font Size** [number]: The font size of the text picture. A number between 0 and 108 (0 uses default font size).

- **Font Style** [list]: The font style of the text picture.

- **Text Color** [string]: The main color of the text picture. Can be #HEX, rgba() or a color code from the window skin (0 to 31).

- **Outline Color** [string]: The outline color of the text picture. Can be #HEX, rgba() or a color code from the window skin (0 to 31).

- **Outline Width** [number]: The outline width of the text picture. A number between 0 and 12.

- **Common Event**: On Mouse Enter [common event]: The common event to call when the picture is hovered by the mouse.

- **Common Event**: On Mouse Exit [common event]: The common event to call when the picture is no more hovered by the mouse.

- **Common Event: On Press** [common event]: The common event to call when the picture is pressed.

- **Common Event: On Click** [common event]: The common event to call when the picture is clicked.

### Create Animation

Displays a sprite as an animated picture.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Sprite** [file]: The sprite file used as a picture.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Scale Width** [number]: The scale width of the picture. A number between 0 and 2000.

- **Scale Height** [number]: The scale height of the picture. A number between 0 and 2000.

- **Rotation Angle** [number]: The rotation angle of the picture. A number between 0 and 360.

- **Opacity** [number]: The opacity of the picture. A number between 0 and 255.

- **Blend Mode** [list]: The blend mode of the picture.

- **Animation Frames** [number]: The number of frames that make up the animated picture. A number between 1 and 100.

- **Animation Duration** [boolean]: The number of frames the animated picture will take to complete a full animation. A number between 0 and 3600.

- **Animation Auto** [bolean]: Indicates whether the animation starts automatically or not.

- **Common Event**: On Mouse Enter [common event]: The common event to call when the picture is hovered by the mouse.

- **Common Event**: On Mouse Exit [common event]: The common event to call when the picture is no more hovered by the mouse.

- **Common Event: On Press** [common event]: The common event to call when the picture is pressed.

- **Common Event: On Click** [common event]: The common event to call when the picture is clicked.

### Erase

Erases a picture

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

### Reset

Resets the picture to its original state.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Transform

Transforms the picture position, scale, rotation, opacity and tint.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Scale Width** [number]: The scale width of the picture. A number between 0 and 2000.

- **Scale Height** [number]: The scale height of the picture. A number between 0 and 2000.

- **Rotation Angle** [number]: The rotation angle of the picture. A number between -360 and 360.

- **Opacity** [number]: The opacity of the picture. A number between 0 and 255.

- **Red** [number]: The red tone. A number between -255 and 255.

- **Green** [number]: The green tone. A number between -255 and 255.

- **Blue** [number]: The blue tone. A number between -255 and 255.

- **Grey** [number]: The grey tone. A number between 0 and 255.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Move

Changes the picture position.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Move Type** [list]: The type of the picture move (relative = current state, or absolute = initial state).

- **Position X** [string]: The X position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Position Y** [string]: The Y position of the picture relative to the screen. Can use a variable v[id] or a number in [pixel].

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Scale

Changes the picture scale.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Scale Type** [list]: The type of the picture scale (relative = current state, or absolute = initial state).

- **Scale Width** [number]: The scale width of the picture. A number between 0 and 2000.

- **Scale Height** [number]: The scale height of the picture. A number between 0 and 2000.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Rotate

Changes the picture angle.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Rotation Type** [list]: The type of the picture rotation (relative = current state, or absolute = initial state).

- **Rotation Angle** [number]: The rotation angle of the picture. A number between -360 and 360.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Start Spinning

Starts spinning the picture (continuous rotation).

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Picture Origin** [list]: The origin point for transforming the picture.

- **Rotation Speed** [number]: The rotation speed of the picture. A number between -90 and 90.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Stop Spinning

Stops spinning the picture (continuous rotation).

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Flip Vertical

Flips the picture vertically.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Flip Horizontal

Flips the picture horizontally.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Fade

Fades the picture (opacity = x).

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Opacity** [number]: The opacity of the picture. A number between 0 and 255.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Fade In

Fades in the picture (opacity = 255).

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Fade Out

Fades out the picture (opacity = 0).

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Easing Type** [list]: The easing type of the picture transformation.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Tint

Changes the picture tone.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

- **Red** [number]: The red tone. A number between -255 and 255.

- **Green** [number]: The green tone. A number between -255 and 255.

- **Blue** [number]: The blue tone. A number between -255 and 255.

- **Grey** [number]: The grey tone. A number between 0 and 255.

- **Duration** [number]: The number of frames that the picture will take to finish the transformation. A number between 0 and 3600.

- **Wait** [boolean]: Indicates whether the current event will wait the picture to finish the transformation or not.

### Start Animation

Starts the animation of an animated picture.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

### Stop Animation

Stops the animation of an animated picture.

- **Picture ID** [string]: The picture ID. Can use a variable v[id] or a number between 1 and 100.

## Notetags

_This plugin doesn't use notetags._

## Script Calls

To manipulate the activation/deactivation of animated pictures (read the plugin code for more information):

- **$gameScreen.apsStartAnimation(number pictureId)** [return void]: Start the animation of the animated picture.

- **$gameScreen.apsStopAnimation(number pictureId)** [return void]: Stop the animation of the animated picture.

- **$gameScreen.apsIsAnimated(number pictureId)** [return boolean]: Return the indication whether the animated picture is animated or not.

## Rewriting Core Functions

**New objects:**
- APS_Game_Picture
- APS_Window_Picture

**New properties:**
- None

**New functions:**
- Game_Screen.prototype.apsCreatePicture
- Game_Screen.prototype.apsErasePicture
- Game_Screen.prototype.apsBindPictureCE
- Game_Screen.prototype.apsResetPicture
- Game_Screen.prototype.apsTransformPicture
- Game_Screen.prototype.apsMovePicture
- Game_Screen.prototype.apsScalePicture
- Game_Screen.prototype.apsRotatePicture
- Game_Screen.prototype.apsSpinPicture
- Game_Screen.prototype.apsFadePicture
- Game_Screen.prototype.apsTintPicture
- Game_Screen.prototype.apsStartAnimation
- Game_Screen.prototype.apsStopAnimation
- Game_Screen.prototype.apsIsAnimated
- Sprite_Picture.prototype.apsLoadBitmapImage
- Sprite_Picture.prototype.apsLoadBitmapText
- Sprite_Picture.prototype.apsLoadBitmapAnimation
- Spriteset_Base.prototype.apsIsAnySpritePressed

**Overwrite declarations:**
- Scene_Map.prototype.isAnyButtonPressed
- Sprite_Picture.prototype.update
- Sprite_Picture.prototype.updateBitmap
- Sprite_Picture.prototype.updateSize (new, but was implemented as an overwritten function)
- Sprite_Picture.prototype.updateFrame (new, but was implemented as an overwritten function)
- Sprite_Picture.prototype.updateOrigin
- Sprite_Picture.prototype.isClickEnabled
- Sprite_Picture.prototype.onMouseEnter
- Sprite_Picture.prototype.onMouseExit
- Sprite_Picture.prototype.onPress
- Sprite_Picture.prototype.onClick

**Destructive declarations:**
- None
